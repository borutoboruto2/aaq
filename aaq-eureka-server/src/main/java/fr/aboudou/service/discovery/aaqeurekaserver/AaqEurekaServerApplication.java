package fr.aboudou.service.discovery.aaqeurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class AaqEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AaqEurekaServerApplication.class, args);
	}

}
