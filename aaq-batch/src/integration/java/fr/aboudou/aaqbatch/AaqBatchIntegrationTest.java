package fr.aboudou.aaqbatch;

import fr.aboudou.aaqbatch.config.AaqBatchConfiguration;
import fr.aboudou.aaqbatch.config.TestConfig;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;

/**
 * Integration test of Aaq data integration test
 *
 * @author sareaboudousamadou.
 */
@SpringBatchTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AaqBatchConfiguration.class, TestConfig.class})
@Sql(executionPhase= Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts={"/org/springframework/batch/core/schema-drop-h2.sql",
                "classpath:/org/springframework/batch/core/schema-h2.sql"})
public class AaqBatchIntegrationTest {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    private JobRepositoryTestUtils jobRepositoryTestUtils;

    private JdbcOperations jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @BeforeEach
    public void clearJobExecutions() {
        this.jobRepositoryTestUtils.removeJobExecutions();

    }

    @Test
    @DisplayName("When aaq batch is launched then exit status should be completed")
    public void aaqJobTest() throws Exception {
        // given
        JobParameters jobParameters = this.jobLauncherTestUtils.getUniqueJobParameters();

        // when
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob(jobParameters);

        // then
        Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
}
