package fr.aboudou.aaqbatch.editors;

import java.beans.PropertyEditorSupport;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Custom editor for {@link java.time.LocalDateTime}
 *
 * @author sareaboudousamadou.
 */
public class LocalDateEditorEditor extends PropertyEditorSupport {
    @Override
    public String getAsText() {
        return super.getAsText();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        super.setValue(LocalDateTime.parse(text, DateTimeFormatter.ISO_ZONED_DATE_TIME));
    }
}
