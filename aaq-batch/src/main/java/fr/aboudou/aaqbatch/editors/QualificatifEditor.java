package fr.aboudou.aaqbatch.editors;

import fr.aboudou.aaqbatch.enums.Qualificatif;
import java.beans.PropertyEditorSupport;

/**
 * Custom editor for {@link Qualificatif} enumeration
 *
 * @author sareaboudousamadou.
 */
public class QualificatifEditor extends PropertyEditorSupport {
    @Override
    public String getAsText() {
        return super.getAsText();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        super.setValue(Qualificatif.valueOf(text));
    }
}
