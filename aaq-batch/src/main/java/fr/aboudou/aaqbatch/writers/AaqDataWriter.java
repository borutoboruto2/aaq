package fr.aboudou.aaqbatch.writers;

import fr.aboudou.aaqbatch.config.AaqBatchConfiguration;
import fr.aboudou.aaqbatch.model.AirData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * Writer of air data information in database
 *
 * @author sareaboudousamadou.
 */
public class AaqDataWriter implements ItemWriter<AirData> {

    private static final Logger logger = LoggerFactory.getLogger(AaqBatchConfiguration.class);

    @Override
    public void write(List<? extends AirData> datas) throws Exception {
        datas.stream().forEach(data -> logger.info("Air data " + data.getQualificatif()));
    }
}
