package fr.aboudou.aaqbatch.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum of different quality air
 *
 * @author sareaboudousamadou.
 */
public enum Qualificatif {

    BON("Bon"), MEDIOCRE("Médiocre"), MOYEN("Moyen"), TRES_BON("Très bon");
    private String label;

      Qualificatif(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    private static final Map<String, Qualificatif> BY_LABEL = new HashMap<>();

    static {
        for (Qualificatif e: values()) {
            BY_LABEL.put(e.label, e);
        }
    }


    public static Qualificatif valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
