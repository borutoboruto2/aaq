package fr.aboudou.aaqbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class AaqBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(AaqBatchApplication.class, args);
	}

}
