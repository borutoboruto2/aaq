package fr.aboudou.aaqbatch.config;

import fr.aboudou.aaqbatch.editors.LocalDateEditorEditor;
import fr.aboudou.aaqbatch.editors.QualificatifEditor;
import fr.aboudou.aaqbatch.model.AirData;
import fr.aboudou.aaqbatch.writers.AaqDataWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import java.beans.PropertyEditor;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration class for Aaq batch
 *
 * @author sareaboudousamadou.
 */
@Configuration
@EnableBatchProcessing
public class AaqBatchConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(AaqBatchConfiguration.class);

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Value("${aaq.input.file.path}")
    private String aaqInputFilePath;

    private static final String[] NAMES =
            new String[]{"x","y","nomCommune","dateEcheance","valeur","qualificatifReadFromFile","source","oidEsri"};

    private static final byte FIRST_LINE = 1;

    /**
     * Reader of air data quality data
     *
     * @return List of AirData read frrom input file
     */
    @Bean
    public FlatFileItemReader<AirData> airDataReader() {

        return new FlatFileItemReaderBuilder<AirData>()
                .name("airDataReader")
                .delimited().names(NAMES)
                .resource(new FileSystemResource(aaqInputFilePath))
                .fieldSetMapper(new BeanWrapperFieldSetMapper<AirData>(){{
                    setTargetType(AirData.class);
                    setCustomEditors(retreiveCustomEditors());}})
                .linesToSkip(FIRST_LINE)
                .build();
    }

    /**
     * Writer of air data quality
     */
    @Bean
    public ItemWriter<AirData> airDataWriter() {
        return new AaqDataWriter();
    }


    @Bean
    public Job importAaqDataJob(Step firstStep) {
        return jobBuilderFactory.get("importAaqDataJob")
                .incrementer(new RunIdIncrementer())
                .flow(firstStep)
                .end()
                .build();
    }

    @Bean
    public Step firstStep(ItemWriter<AirData> airDataWriter) {
        TaskletStep firstStep = stepBuilderFactory.get("firstStep")
                .<AirData, AirData>chunk(100)
                .reader(airDataReader())
                .writer(airDataWriter)
                .build();
        return firstStep;
    }


    private Map<Object, PropertyEditor> retreiveCustomEditors() {
        Map<Object, PropertyEditor> customsEditor = new HashMap<>();
        customsEditor.put("fr.aboudou.aaqbatch.enums.Qualificatif", new QualificatifEditor());
        customsEditor.put("java.time.LocalDateTime", new LocalDateEditorEditor());
        return customsEditor;
    }

}
