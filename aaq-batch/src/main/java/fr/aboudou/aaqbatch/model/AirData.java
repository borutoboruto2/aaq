package fr.aboudou.aaqbatch.model;

import fr.aboudou.aaqbatch.enums.Qualificatif;
import lombok.*;

import java.time.LocalDateTime;

/**
 * Information representing air data quality
 *
 * @author sareaboudousamadou.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirData {

    private float x;
    private float y;
    private String nomCommune;
    private LocalDateTime dateEcheance;
    private int valeur;
    private Qualificatif qualificatif;
    private String qualificatifReadFromFile;
    private String source;
    private int oidEsri;

    public Qualificatif getQualificatif() {
        return Qualificatif.valueOfLabel(this.qualificatifReadFromFile);
    }
}
