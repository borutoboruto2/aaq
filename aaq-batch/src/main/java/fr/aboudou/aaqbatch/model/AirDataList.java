package fr.aboudou.aaqbatch.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent list of air data information
 *
 * @author sareaboudousamadou.
 */
public class AirDataList {

    public List<AirData> getAirDataList() {
        return airDataList;
    }

    public void setAirDataList(List<AirData> airDataList) {
        this.airDataList = airDataList;
    }

    private List<AirData> airDataList = new ArrayList<>();
}
