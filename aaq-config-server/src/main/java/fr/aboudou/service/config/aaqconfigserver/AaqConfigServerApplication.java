package fr.aboudou.service.config.aaqconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class AaqConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AaqConfigServerApplication.class, args);
	}

}
